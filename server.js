var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var movimientosv2JSON=require('./movimientosv2.json');
const bcrypt=require('bcryptjs');
const rounds = 10;

//var bodyparser=require('body-parser');
//app.use(bodyparser.json());

app.listen(port);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson=require("request-json");
var urlMLabRaiz="https://api.mlab.com/api/1/databases/dsanchez/collections";
var apiKeyMLab="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;
var usuarioMLabRaiz;
var ProductoMLabRaiz;
var urlUsuarios = "https://api.mlab.com/api/1/databases/dsanchez/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlClientes = "https://api.mlab.com/api/1/databases/dsanchez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlProductos = "https://api.mlab.com/api/1/databases/dsanchez/collections/Productos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab=requestjson.createClient(urlClientes);
var usuarioMLab=requestjson.createClient(urlUsuarios);
var productosMLab=requestjson.createClient(urlProductos);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/', function (req, res){
  res.send('Hemos recibido su peticion post');
});

app.put('/', function (req, res){
  res.send('Hemos recibido su peticion put');
});

app.delete('/', function (req, res){
  res.send('Hemos recibido su peticion deletecambiada');
});

app.get('/Clientes/:idcliente', function(req, res) {
  res.send('Aqui tiene al cliente numero:' + req.params.idcliente);
});

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosv2JSON);
});

app.get('/v2/Movimientos/:id', function(req, res) {
  console.log(req.params.id);
  res.send(movimientosv2JSON[req.params.id]);
});

app.get('/v2/Movimientosquery', function(req, res) {
  console.log(req.query);
  //res.send('Recibido: '+req.query.nombre);
  res.send('Recibido: '+req.query);
});

app.post('/v2/Movimientos', function(req, res){
  var nuevo = req.body;
  nuevo.id = movimientosv2JSON.length+1;
  movimientosv2JSON.push(nuevo);
  res.send('Movimiento dado de alta con id: '+nuevo.id);
});

app.get('/Clientes', function(req, res){
  //var clienteMLab=requestjson.createClient(urlClientes);

  clienteMLab.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  });
});

app.post('/Clientes', function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body){
    res.send(body);
  });
});

app.post('/login', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var usuario = req.body.usuario;
  var password = req.body.password;
  //var query = 'q={"email":"'+email+'","password":"'+password+'"}';
  var query = 'q={"usuario":"'+usuario+'"}';
  console.log('QUERY: '+query);

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz+"/Usuarios?"+apiKeyMLab+"&"+query);
  console.log('URL: '+urlMLabRaiz+"/Usuarios?"+apiKeyMLab+"&"+query);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length==1){// login OK
        //Cargando Hash desde BD
        bcrypt.compare(password, body[0].password, function(err, isMatch){
          if(err){
            console.log('Error al procesar comparación');
            res.status(500).send('Error en identificación del usuario');
          }else if(!isMatch){
            console.log('Password Erroneo '+isMatch);
          }else{
            console.log('Password Correcto!');
            res.status(200).send('Usuario Valido: '+JSON.stringify(body[0]));
          }
        });
      }else{
        res.status(400).send('Usuario no encontrado');
      }
    }
  });
});

app.post('/usuarios/alta', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var usuario = req.body.usuario;
  var nombre = req.body.nombre;
  var apellidos = req.body.apellidos;
  var email = req.body.email;
  var password = req.body.password;

  bcrypt.genSalt(rounds, function(err, salt){
    if(err){
      console.log('Error generando token');
    }else{
      bcrypt.hash(password, salt, function(err, hash){
        if(err){
          console.log('Error en generación de Hash');
        }else{
          //Almacenar Hash en BD
          console.log('Generado: '+hash);
          req.body.password = hash;
          usuarioMLabRaiz = requestjson.createClient(urlMLabRaiz+"/Usuarios?"+apiKeyMLab);
          usuarioMLabRaiz.post('',req.body, function(err, resM, body){
            console.log('Codigo Retornado: '+resM.statusCode);
            if(resM.statusCode==200){
              res.status(200).send('Usuario Registrado Exitosamente');
            }else{
              res.status(400).send('No se pudo almacenar el usuario'+JSON.stringify(resM));
            }
          });

        }
      })
    }
  });
});

app.post('/productos/alta', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  console.log('Registrando nota');
  usuarioMLabRaiz = requestjson.createClient(urlMLabRaiz+"/Productos?"+apiKeyMLab);
  console.log("DATA: "+JSON.stringify(req.body));
  usuarioMLabRaiz.post('', req.body, function(err, resM, body){
    console.log('Codigo Retornado: '+resM.statusCode);
    if(resM.statusCode==200){
      res.status(200).send('Producto Registrado Exitosamente');
    }else{
      res.status(400).send('No se pudo almacenar el producto'+JSON.stringify(resM));
    }
  });
});
